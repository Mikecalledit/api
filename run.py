#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import app

if __name__ == "__main__":
    app.run(host=app.config['HOST'])
