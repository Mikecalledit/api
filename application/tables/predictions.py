#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import db

class PredictionsTable(db.Model):
    __tablename__ = 'predictions'
    uuid = db.Column('uuid', db.String, primary_key=True)
    user_uuid = db.Column('user_uuid', db.String, db.ForeignKey("users.uuid"))
    thing = db.Column('thing', db.String(255), unique=True, nullable=False)
    prediction = db.Column('prediction', db.String(255), nullable=False)
    by = db.Column('by', db.Date())
    created = db.Column('created', db.DateTime())
