#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import db, bcrypt, login_manager

class UsersTable(db.Model):
    __tablename__ = 'users'
    uuid = db.Column('uuid', db.String, primary_key=True)
    email = db.Column('email', db.String(50), unique=True, nullable=False)
    name = db.Column('name', db.String(50), nullable=False)
    _password = db.Column('password', db.String(50), nullable=False)
    status = db.Column('status', db.Enum('active', 'inactive'), default='active')
    role = db.Column('role', db.Enum('superadmin', 'superuser', 'admin', 'user'), default='user')

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def is_authenticated(self):
        return True

    def get_id(self):
        return (self.uuid)

    def is_active(self):
        return self.status == 'active'

    def is_admin(self):
        return self.role in ['superadmin', 'superuser', 'admin']

    def is_superuser(self):
        return self.role in ['superadmin', 'superuser']

    def is_superadmin(self):
        return self.role == 'superadmin'

    def is_anonymous(self):
        return False

    def __repr__(self):
        return '<User {0}::{1}>'.format(self.uuid, self.email)

@login_manager.user_loader
def load_user(userid):
    return UsersTable.query.filter(UsersTable.uuid == userid).first()
