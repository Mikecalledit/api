#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import app, api
from flask import request, url_for
from flask.ext.restful import fields, marshal, abort
from flask.ext.login import current_user
from application.tables.users import UsersTable
from controllers import Controller, ListController

class UserFields(fields.Raw):
    def output(self, key, data):
        return [
            {'name': 'name', 'prompt': 'Name', 'value': data.name},
            {'name': 'email', 'prompt': 'Email', 'value': data.email},
            {'name': 'uuid', 'prompt': 'UUID', 'value': data.uuid},
        ]

user_template =  {
    'data': [
        {'name': 'name', 'prompt': 'Name', 'value': ''},
        {'name': 'email', 'prompt': 'Email', 'value': ''},
        {'name': 'uuid', 'prompt': 'UUID', 'value': ''},
        {'name': 'password', 'prompt': 'Password', 'value': ''},
    ]
}

user_queries = [
    {
        'rel': 'search',
        'href': '/users/search',
        'prompt': 'Search',
        'data': [{'name': 'search', 'value': ''}]
    }
]

user_fields = {
    'href': fields.Url('user_ep'),
    'data': UserFields
}

user_collection = 'users_ep'
user_not_found = "User {} does not exist"

class UserController(Controller):
    def __init__(self):
        ''' call the parent __init__ and then initialise instance variables'''
        super(UserController, self).__init__()
        self.marshal_with = user_fields
        self.template = user_template
        self.queries = user_queries
        self.collection_endpoint = user_collection
        self.item_not_found = user_not_found

    '''get a single user'''
    def get_item(self, uuid):
        app.logger.debug('{0}::get({1}'.format(type(self), uuid))
        return UsersTable.query.get(uuid)

class UserListController(ListController):
    def __init__(self):
        ''' call the parent __init__ and then initialise instance variables'''
        super(UserListController, self).__init__()
        self.marshal_with = user_fields
        self.template = user_template
        self.queries = user_queries
        self.collection_endpoint = user_collection
        self.item_not_found = user_not_found

    '''get all the users'''
    def get_items(self):
        '''Fetch a list of users'''
        app.logger.debug('{0}::get()'.format(type(self)))
        return UsersTable.query.order_by(UsersTable.email).all()

