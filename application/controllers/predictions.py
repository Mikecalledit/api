#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import app, api
from flask import request, url_for
from flask.ext.restful import fields, marshal, abort
from flask.ext.login import current_user
from application.tables.predictions import PredictionsTable
from controllers import Controller, ListController

class PredictionFields(fields.Raw):
    def output(self, key, data):
        return [
            {'name': 'thing', 'prompt': 'What', 'value': data.thing},
            {'name': 'prediction', 'prompt': 'Prediction', 'value': data.prediction},
            {'name': 'by', 'prompt': 'By', 'value': str(data.by)},
            # {'name': 'score', 'prompt': 'Score', 'value': data.score},
        ]

prediction_template =  {
    'data': [
        {'name': 'thing', 'prompt': 'What', 'value': ''},
        {'name': 'prediction', 'prompt': 'Prediction', 'value': ''},
        {'name': 'by', 'prompt': 'By', 'value': ''},
    ]
}

prediction_queries = [
    {
        'rel': 'search',
        'href': '/predictions/search',
        'prompt': 'Search',
        'data': [{'name': 'search', 'value': ''}]
    }
]

prediction_fields = {
    'href': fields.Url('prediction_ep'),
    'data': PredictionFields
}

prediction_collection = 'predictions_ep'
prediction_not_found = "Prediction {} does not exist"

class PredictionController(Controller):
    def __init__(self):
        ''' call the parent __init__ and then initialise instance variables'''
        super(PredictionController, self).__init__()
        self.marshal_with = prediction_fields
        self.template = prediction_template
        self.queries = prediction_queries
        self.collection_endpoint = prediction_collection
        self.item_not_found = prediction_not_found

    '''get a single user'''
    def get_item(self, uuid):
        app.logger.debug('{0}::get({1}'.format(type(self), uuid))
        return PredictionsTable.query.get(uuid)

class PredictionListController(ListController):
    def __init__(self):
        ''' call the parent __init__ and then initialise instance variables'''
        super(PredictionListController, self).__init__()
        self.marshal_with = prediction_fields
        self.template = prediction_template
        self.queries = prediction_queries
        self.collection_endpoint = prediction_collection
        self.item_not_found = prediction_not_found

    '''get all the predictions'''
    def get_items(self):
        '''Fetch a list of predictions'''
        app.logger.debug('{0}::get()'.format(type(self)))
        return PredictionsTable.query.filter_by(user_uuid=current_user.uuid).order_by(PredictionsTable.by).all()

