#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import api
from flask import make_response
import json

from sessions import SessionsController
from users import UserController, UserListController
from predictions import PredictionController, PredictionListController

api.add_resource(SessionsController, '/sessions', endpoint='sessions_ep')
api.add_resource(UserListController, '/users', endpoint='users_ep')
api.add_resource(UserController, '/users/<string:uuid>', endpoint='user_ep')
api.add_resource(PredictionListController, '/predictions', endpoint='predictions_ep')
api.add_resource(PredictionController, '/predictions/<string:uuid>', endpoint='prediction_ep')

# define a function to generate a collection+json object from the response object, and hook it into restful
@api.representation('application/vnd.collection+json')
def output_cj(data, code, headers=None):
    cj = {
        "collection": {
            "version": "1.0",
            "href": data.get('href', None),
        }
    }

    # add a links fields only if one has been returned
    if 'links' in data:
        cj['links'] = data['links']

    # add an items field only if one has been returned
    if 'items' in data and data['items'] is not None:
        cj['items'] = data['items'] if isinstance(data['items'], list) else [data['items']]

    # add a queries field only if one has been returned
    if 'queries' in data and data['queries'] is not None:
        cj['queries'] = data['queries']

    # add a template field only if one has been returned
    if 'template' in data and data['template'] is not None:
        cj['template'] = data['template']

    # add an error field only if one has been returned
    if 'error' in data:
        cj['error'] = data['error']
    elif code >= 400 and 'message' in data:
        # convert a standard Flask error into a CJ error object
        cj['error'] = {
            'message': data['message'],
            'code': data.get('status', code)
        }

    # cj['original'] = data

    resp = make_response(json.dumps(cj), code)
    resp.headers.extend(headers or {})

    return resp
