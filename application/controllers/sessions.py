#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import app
from flask import request, url_for
from flask.ext.login import login_user, logout_user, current_user
from json import loads
from application.tables.users import UsersTable
from controllers import BaseController

session_template =  {
    'data': [
        {'name': 'email', 'prompt': 'Email', 'value': ''},
        {'name': 'password', 'prompt': 'Password', 'value': ''},
    ]
}

session_collection = 'sessions_ep'

class SessionsController(BaseController):
    def __init__(self):
        ''' call the parent __init__ and then initialise instance variables'''
        super(SessionsController, self).__init__()
        self.template = session_template
        self.collection_endpoint = session_collection

    def post(self):
        app.logger.debug('{0}::post()'.format(type(self)))
        try:
            data = loads(request.data)
            if 'email' not in data or 'password' not in data:
                raise TypeError('Must supply email and password')

            user = UsersTable.query.filter(UsersTable.email == data['email']).first()

            if user is not None and user.is_active() and user.check_password(data['password']) and login_user(user):
                headers = {
                    'Location': '/api/users'
                }
                return self.response(None, 302, headers)
            else:
                return self.error('Invalid email or password', 401)

        except ValueError as e:
            return self.error(e.message, 400)

        except TypeError as e:
            return self.error(e.message, 400)

    def delete(self):
        app.logger.debug('{0}::delete()'.format(type(self)))
        logout_user()
        return self.response(None, 200)
