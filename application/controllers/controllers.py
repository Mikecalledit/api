#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from application import app
from flask import url_for, request
from flask.ext.restful import Resource, marshal, abort
from flask.ext.login import current_user
from sqlalchemy.exc import DataError

class BaseController(Resource):
    def __init__(self):
        self.marshal_with = None
        self.template = None
        self.queries = None
        self.collection_endpoint = None
        self.item_not_found = "Item {} does not exist"
        self.links = []

    def response(self, data, code=200, headers=None):
        '''put together all the data needed to be returned'''
        app.logger.debug('{0}::response({1}) using {2}'.format(type(self), data, self.marshal_with))
        return {
            'links': self.get_links(),
            'href': self.get_collection_link(),
            'items': self.marshal_item(data) if data is not None else None,
            'template': self.get_template(),
            'queries': self.get_queries()
        }, code, headers

    def error(self, message, code, headers=None):
        '''put together an error object'''
        return {
            'href': self.get_collection_link(),
            'links': self.get_links(),
            'template': self.get_template(),
            'error': {
                'message': message,
                'code': code
            }
        }, code, headers

    def marshal_item(self, data):
        '''extract the required fields from the table object'''
        app.logger.debug('{0}::marshal_item({1}) using {2}'.format(type(self), data, self.marshal_with))
        return marshal(data, self.marshal_with) if self.marshal_with is not None else ''

    def get_template(self):
        '''get the template for the instantiated type'''
        app.logger.debug('{0}::get_template() using {1}'.format(type(self), self.template))
        return self.template

    def get_queries(self):
        '''get the available queries for the instantiated type'''
        app.logger.debug('{0}::get_queries() using {1}'.format(type(self), self.queries))
        return self.queries

    def get_collection_link(self):
        '''get the url for the collection of the instantiated type'''
        app.logger.debug('{0}::get_collection_link() using {1}'.format(type(self), self.collection_endpoint))
        return url_for(self.collection_endpoint) if self.collection_endpoint is not None else ''

    def get_links(self):
        '''get the available links from this point'''
        app.logger.debug('{0}::get_links() using {1}'.format(type(self), []))

        if current_user.is_authenticated():
            # show links valid for all users
            self.links.append(self.make_link('Logout', 'logout', url_for('sessions_ep')))
            self.links.append(self.make_link('Feed', 'feed', url_for('predictions_ep')))
        else:
            self.links.append(self.make_link('Login', 'login', url_for('sessions_ep')))

        return self.links

    def make_link(self, prompt, name, url):
        return {
            'rel': name,
            'prompt': prompt,
            'href': url
        }

    def is_authorized(self, user):
        '''default authorization check - is user logged in?'''
        # app.logger.debug('checking if "{0}" is allowed to {1}'.format(user, request.method))
        return user.is_authenticated()

    def access_denied(self):
        return self.error('Access denied', 401)

class Controller(BaseController):
    '''Show, modify or delete a single item'''
    def get(self, uuid):
        '''Fetch a single item'''
        app.logger.debug('{0}::get({1})'.format(type(self), uuid))

        # authorised?
        if self.is_authorized(current_user) is False:
            return self.access_denied()

        try:
            item = self.get_item(uuid)
        except DataError, e:
            return self.error(e.message, 400)
        if item is None:
            return self.error(self.item_not_found.format(uuid), 404)

        return self.response(item)

    def put(self, uuid):
        '''Modify a single item'''

        # authorised?
        if self.is_authorized(current_user) is False:
            return self.access_denied()
            
        app.logger.debug('{0}::put({1})'.format(type(self), uuid))
        self._model.save(uuid, {'data': 'test'})
        return {'data': 'test'}

    def delete(self, uuid):
        '''Delete a single item'''

        # authorised?
        if self.is_authorized(current_user) is False:
            return self.access_denied()
            
        app.logger.debug('{0}::delete({1})'.format(type(self), uuid))
        self._model.delete(uuid)
        return {}

class ListController(BaseController):
    '''Fetch a list of items, or add a new item'''
    def get(self):
        '''Fetch a list of items'''

        # authorised?
        app.logger.debug('{0}::get(), user:{1}'.format(type(self), current_user))
        if self.is_authorized(current_user) is False:
            return self.access_denied()
            
        app.logger.debug('{0}::get_all'.format(type(self)))
        items = self.get_items()
        return self.response(items)

    def post(self):
        '''Add a new item'''

        # authorised?
        if self.is_authorized(current_user) is False:
            return self.access_denied()
            
        app.logger.debug('{0}::post'.format(type(self)))
        self._model.save(None, {'data': 'test'})
        return {'data': 'test'}

