#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.restful import Api
from flask.ext.bcrypt import Bcrypt
from flask.ext.login import LoginManager
from flask.ext.mail import Mail


app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

# add API
api = Api(app, prefix='/api')

# create Login Manager
login_manager = LoginManager()
login_manager.setup_app(app)

# add bcrypt
bcrypt = Bcrypt(app)

# add email
mail = Mail(app)

# configure logging
import logging
from logging import Formatter
from logging.handlers import TimedRotatingFileHandler
file_handler = TimedRotatingFileHandler('logs/application.log', when='midnight', backupCount=10)
file_handler.setLevel(app.config['LOGLEVEL'])
file_handler.setFormatter(Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
))
app.logger.addHandler(file_handler)
app.logger.setLevel(app.config['LOGLEVEL'])
app.logger.info('Application started')

# add additional modules to be logged
loggers = [
    # logging.getLogger('sqlalchemy'),
    logging.getLogger('ysm')
]

for logger in loggers:
    logger.addHandler(file_handler)
    logger.setLevel(app.config['LOGLEVEL'])
    app.logger.debug('Adding handler to logger: %r' % (logger.name))

# app.logger.debug('Configuration: %r' % (app.config))
import application.controllers
