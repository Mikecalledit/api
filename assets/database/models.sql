---User that can login and manage predictions, or admin user to manage system
CREATE TYPE user_status AS ENUM ('active', 'inactive');
CREATE TYPE user_role AS ENUM ('admin', 'user');
CREATE TABLE IF NOT EXISTS users (
  uuid                UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  email               varchar(50) UNIQUE,
  name                varchar(50),
  password            char(60) NOT NULL,
  role                user_role,
  status              user_status
);

---Prediction made by a user
CREATE TABLE IF NOT EXISTS predictions (
  uuid                UUID UNIQUE DEFAULT uuid_generate_v4(),
  user_uuid           UUID REFERENCES users(uuid) ON DELETE CASCADE,
  thing               varchar(255) NOT NULL,
  prediction          varchar(255) NOT NULL,
  by                  date,
  created             timestamp NOT NULL
);
CREATE INDEX prediction_user ON predictions(user_uuid);
CREATE INDEX prediction_by ON predictions(by);
CREATE INDEX prediction_created ON predictions(created);

---Score for each prediction, and who voted it up/down
CREATE TABLE IF NOT EXISTS scores (
  user_uuid           UUID REFERENCES users(uuid) ON DELETE CASCADE,
  prediction_uuid     UUID REFERENCES predictions(uuid) ON DELETE CASCADE,
  scores              INTEGER DEFAULT 0
);
CREATE UNIQUE INDEX scores_uuid ON scores (user_uuid, prediction_uuid);
CREATE UNIQUE INDEX scores_prediction ON scores (prediction_uuid);
