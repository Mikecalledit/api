-- drop tables
DROP TABLE IF EXISTS scores;
DROP TABLE IF EXISTS predictions;
DROP TABLE IF EXISTS users;

-- drop types
DROP TYPE IF EXISTS user_status;
DROP TYPE IF EXISTS user_role;
