##Called It API
###Introduction

The Called It API is designed to be a ReST API. Thus it has:-

  - well defined endpoints for resources
  - supports verbs for the actions (in this case HTTP verbs)
  - returns data is a well defined hypermedia format (in this case application/vnd.collection+json)
  - server maintains state

The endpoints defined below simply list what each does. Examining the returned hypermedia response will provide more (and more current) information about each endpoint.

###Endpoints

All endpoints expect supplied payload to be a valid json string. All responses are json.

####/api/sessions
<table width=100%>
	<tr>
		<th width=10%>Action</th><th width=70%>Description</th><th width=20%>Status</th>
	</tr>
	<tr>
		<td>POST</td><td>Create a new session. Use the supplied cookie to authenticate all further requests</td><td>Implemented</td>
	</tr>
	<tr>
		<td>DELETE</td><td>Delete the current session. The session is invalidated and the cookie will no longer work</td><td>Implemented</td>
	</tr>
</table>

####/api/users
<table width=100%>
	<tr>
		<th width=10%>Action</th><th width=70%>Description</th><th width=20%>Status</th>
	</tr>
	<tr>
		<td>GET</td><td>Get a list of users</td><td>Implemented</td>
	</tr>
	<tr>
		<td>POST</td><td>Add a new user</td><td>In progress</td>
	</tr>
</table>

####/api/users/&lt;UUID&gt;
<table width=100%>
	<tr>
		<th width=10%>Action</th><th width=70%>Description</th><th width=20%>Status</th>
	</tr>
	<tr>
		<td>GET</td><td>Get detail of the user with identified by UUID</td><td>Implemented</td>
	</tr>
	<tr>
		<td>PUT</td><td>Update the user identified by UUID using the supplied data</td><td>In progress</td>
	</tr>
	<tr>
		<td>DELETE</td><td>delete the user identified by UUID</td><td>In progress</td>
	</tr>
</table>

####/api/predictions
<table width=100%>
	<tr>
		<th width=10%>Action</th><th width=70%>Description</th><th width=20%>Status</th>
	</tr>
	<tr>
		<td>GET</td><td>Get a list of predictions</td><td>Implemented</td>
	</tr>
	<tr>
		<td>POST</td><td>Add a new prediction</td><td>In progress</td>
	</tr>
</table>

####/api/predictions/&lt;UUID&gt;
<table width=100%>
	<tr>
		<th width=10%>Action</th><th width=70%>Description</th><th width=20%>Status</th>
	</tr>
	<tr>
		<td>GET</td><td>Get detail of the prediction with identified by UUID</td><td>Implemented</td>
	</tr>
	<tr>
		<td>PUT</td><td>Update the prediction identified by UUID using the supplied data</td><td>In progress</td>
	</tr>
	<tr>
		<td>DELETE</td><td>Delete the prediction identified by UUID</td><td>In progress</td>
	</tr>
</table>
