#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2014 CalledIt
#
# Author: Abdul Gani
#
#

import os
import logging

_basedir = os.path.abspath(os.path.dirname(__file__))

# this is the FLASK debug setting - never set this to True on a server accessible via the Internet
# - doing so will cause the server to be compromised by a remote attacker
DEBUG = True

# IP to bind to, or 0.0.0.0 for all IP's
HOST = '0.0.0.0'
LOGLEVEL = logging.DEBUG

ADMINS = frozenset(['abdulg@gmail.com'])
SECRET_KEY = 'RTHRXb8nFvrfXQNh9LPBfrDVDFesgF'

# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'app.db')
SQLALCHEMY_DATABASE_URI = 'postgres://calledit:mycalleditpass@localhost/calledit'
DATABASE_CONNECT_OPTIONS = {}

THREADS_PER_PAGE = 8

WTF_CSRF_ENABLED = True
CSRF_SESSION_KEY = "h7LoN774QBf6G6F3fGqy9JcrM33UoU"

RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = 'blahblahblahblahblahblahblahblahblah'
RECAPTCHA_PRIVATE_KEY = 'blahblahblahblahblahblahprivate'
RECAPTCHA_OPTIONS = {'theme': 'white'}

MAIL_DEBUG = False
DEFAULT_MAIL_SENDER = 'CalledIT <abdul@gmail.com>'

APPLICATION = {
	'version': '1.0',
	'title': 'Called It API',
	'description': 'Make predictions and brag about them'
}
